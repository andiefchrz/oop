<?php

require_once('animal.php');

class Frog extends Animal
{
	public $legs = 4;
	public $name = "buduk";

	public function jump()
	{
		echo "Hop Hop";
	}
}